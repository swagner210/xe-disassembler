#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <bitset>
#include <cctype> //tolower
#include <algorithm>
#include <iomanip>
#include "functions.cpp"

using namespace std;


void filecheck(string filename){
	fstream testfile(filename);
	if(!(testfile.is_open())){
		exit(EXIT_FAILURE);
	}
}

int base;
bool boolbase;


int main(int argc,char *argv[]){
	map<string, string> symmap;
	map<string, string> litmap;
	vector<string> valuevecstring;
	vector<int> valuevec;
    string filecontents;
	string symstr;
	string endadd;
	string startadds;
	bool iflag = false;
	bool nflag = false;
	bool xflag = false;
	bool bflag = false;
	bool pflag = false;
	bool eflag = false;
	int address = 0;
	bool ltorg = false;
	bool BASE = false;
	


	if( argc >= 3){
		cout<< ("To many files.")<<endl;
		exit(EXIT_FAILURE);
	}

    if(argc == 2){
    	string sourcefile(argv[1]);
		std::size_t found = sourcefile.find(".obj");
		if(found != std::string::npos){
			//cout<<"this is the correct file!"<<endl<<endl;
		}else{
			cout<< "this is the wrong file type"<<endl;
			exit(EXIT_FAILURE);
		}	
		
		symstr = sourcefile;
		symstr.replace(found,4,".sym");
		filecheck(sourcefile);
		filecheck(symstr);
		fstream objf(sourcefile);
		fstream symf(symstr);
		

		if(symf.is_open()){
			map<string, string>::iterator itr;
			string line;
			while(getline (symf,line)){
			    if (!(line[line.length()-1] == ':' || line[line.length()-1] == '-')){ 
			        if(line.length()!= 0){
			            if( line.at(0) == ' '){

			                litmap.insert(pair<string,string>(line.substr(9,5),line.substr(24,6))); 
			            }else{
							valuevecstring.push_back(line.substr(8,6));
							//cout<<line.substr(8,6)<<endl;
			                symmap.insert(pair<string,string>(line.substr(0,line.find(" ")),line.substr(8,6)));
			            }
			        }
			    }
			}
			
		
			/*	cout<<"This is the symbol table"<<endl;
			for (itr = symmap.begin(); itr != symmap.end(); ++itr){
        		cout <<'\t' << itr->first
       			 <<'\t' <<itr->second <<'\n';
			}
			
			cout<<endl<<"This is the literal table"<<endl;
			for (itr = litmap.begin(); itr != litmap.end(); ++itr){
        		cout <<'\t' << itr->first
       			 <<'\t' <<itr->second <<'\n';
			}
			cout<<endl;
	*/

			symf.close();
		}
	    
		

		if(objf.is_open()){
			while(getline(objf,filecontents)){
				//cout<<filecontents.at(0)<<endl;
				if(filecontents.at(0) == 'H'){
					//cout<< "This is the header record."<<endl<<filecontents<<endl<<endl;
					string startlabel = filecontents.substr(1,6);
					startadds = filecontents.substr(7,6);
					endadd = filecontents.substr(13,18);
				
					int startadd = inttohex(3,false,startadds);


					cout<< setfill(' ')<<setw(8)<<" "; 
					cout<<setfill(' ')<<setw(6)<<startlabel;
					cout<<setfill(' ')<<setw(2)<<" ";
					cout<<setfill(' ')<<setw(8)<<left<<"START";
					cout<<setfill(' ')<<setw(1)<<" ";
					cout<<setfill(' ')<<setw(6)<<left<<hex<<startadd<<endl;
				}

				if(filecontents.at(0) == 'T'){
					string startadd = filecontents.substr(1,6);
					string oclength = filecontents.substr(7,2);
					string objectcodes = filecontents.substr(9);
					address = inttohex(3,false,startadd);		//Make the address start at the starting address

					string fullopcode;			//This is the full opcode ie: all 6 or 8 numbers					
					string tempopcode;			//this is opcode number
				    string opcode;				//this is worded opcode
					string tempxbpe;			//Use this to check xbpe flags
					string tempdis;				//The displacement of the code
					unsigned int tempdisint;	//Int version of displacement
					int TA;						//the TArget address int version
					string instruction;			
					int tempadd;
					int temp;
					bool neg = false;
					int current = 0;
					string label;
					stringstream labelnum;


					while(current < objectcodes.length()){
						ltorg = false;				//Set LTORG and BASE printouts to false
						BASE = false;

						// Get the opcode from string and dissasemble to correct opcode
						tempopcode = objectcodes.substr(current,2);
						tempopcode = opcodefinder(tempopcode);
						
						//Get the binary version of flags and then set all the flags
						tempxbpe = strtobinary(objectcodes.at(current+2));
						iflag = iflagset(tempopcode);
						nflag = nflagset(tempopcode);		
						xflag = xflagset(tempxbpe);
						bflag = bflagset(tempxbpe);
						pflag = pflagset(tempxbpe);
						eflag = eflagset(tempxbpe);
                
							//see if opcode is on symbol table and set to opcode
						if(searchbyValueB(opcodetable,tempopcode)){						
							opcode = searchbyValueS(opcodetable,tempopcode);
						}
						
						if(tempopcode == "4C"){  			//check if opcode is RSUB
							label = "";
							instruction="";
							fullopcode = objectcodes.substr(current,6);
							printtextrecord(address,label,opcode,instruction,fullopcode,xflag,iflag,nflag,eflag,ltorg,BASE);
							
							address = address + 3;
							current += 6;
					   
						//check if opcode is format 1
						}else if(tempopcode == "C4" ||tempopcode == "C0" ||tempopcode == "F4" ||tempopcode == "C8" || tempopcode =="F0" ||tempopcode == "F8"){
							fullopcode = objectcodes.substr(current,2);
							address = address +1;
							current += 2;
						
						//check if opcode is format 2
						}else if( tempopcode == "90" || tempopcode == "B4" || tempopcode == "A0" || tempopcode == "98" || tempopcode == "AC" || tempopcode == "A4" || tempopcode == "A8" || tempopcode == "94" || tempopcode == "B0" || tempopcode == "B8"){
							fullopcode = objectcodes.substr(current,4);
							address = address +2;
							current += 4;
						
						//check if opcode is format 4
						}else if(eflag){
                        	fullopcode = objectcodes.substr(current,8);		//get the full 8 digit opcode to print later
    						tempdis =  objectcodes.substr(current+3,5);		//get the displacement of opcode        
                            tempdisint =inttohex(4,false,tempdis);			//get the int version of that temp dislacement	
							stringstream TAS;						
                            TAS<<setw(6)<<setfill('0')<<hex<<tempdisint;	//Put that int diplacement into string form
                           
                            if(searchbyValueB(symmap,uppercase(TAS.str()))){			//check if diplacement is a symbol
								instruction = searchbyValueS(symmap,uppercase(TAS.str()));
                            }
							if(tempopcode == "68"){						//check to see if opcode is LDB so we can assign BASE
								tempdis =  objectcodes.substr(current+3,5);
								tempdisint =inttohex(4,false,tempdis);
								base = tempdisint;
								BASE = true;				//BASE is now true for print out
							}

							printtextrecord(address,label,opcode,instruction,fullopcode,xflag,iflag,nflag,eflag,ltorg,BASE);


							address = address + 4;		//increase address by 4 and current by 8
							current += 8;

							//opcode is now format 3
						}else{
							fullopcode = objectcodes.substr(current,6);		//get the fullopcode for printout
							tempdis= objectcodes.substr(current+3,3);		//get temp displacement
							if(tempdis.at(0) == 'F'){						//If displacement is negative,make it positive
								tempdis = negativenum(3,tempdis);
								tempdisint = inttohex(3,true,tempdis);
								neg = true;
							}else{
								tempdisint =inttohex(3,false,tempdis);
								neg = false;
							}
						
							stringstream TAS;
							stringstream addstr;
							addstr<<setw(6)<<setfill('0')<<hex<<address;
							
							if(searchbyValueB(litmap,addstr.str())){
								ltorg = true;
								TAS<< uppercase(addstr.str());
								address+=1;
								current+=2;
                             

                          	}else if(tempopcode == "68"){
								BASE = true;
                                base = tempdisint;

							}else if(pflag){										//PC Relative
								//cout<<"The disp is: "<<tempdisint<<endl;
								
								tempadd = address + 3;		
								TA = tempdisint + tempadd;
								TAS<<setw(6)<<setfill('0')<<hex<<TA; //TA.str();
								
							
							}else{												//Base Relative
                                TA = tempdisint + base;
                                TAS<<setw(6)<<setfill('0')<<hex<<TA;
                               // cout<<"This is base "<<TAS.str()<<endl;
							}
							

							if(neg){
								if(searchbyValueB(symmap,uppercase(TAS.str().substr(1,6)))){						//check if symbol
									instruction = searchbyValueS(symmap,uppercase(TAS.str().substr(1,6)));
									//cout<< "The instruction is: "<<instruction<<endl;
								}
								if(searchbyValueB(litmap,TAS.str().substr(1,6))){						//check if literal
									instruction = searchbyValueS(litmap,TAS.str().substr(1,6));
									//cout<< "The instruction is: "<<instruction<<endl;
								}
							}else{
								if(searchbyValueB(litmap,TAS.str())){						//check if literal
									instruction = searchbyValueS(litmap,TAS.str());
									
									//cout<< "The instruction is: "<<instruction<<endl;
								}
								if(searchbyValueB(symmap,uppercase(TAS.str()))){						//check if symbol
										instruction = searchbyValueS(symmap,uppercase(TAS.str()));
									//	cout<< "The instruction is: "<<instruction<<endl;
								}
							}
			
							stringstream labelnum;
						
							labelnum<<setw(6)<<setfill('0')<<hex<<address; //TA.str();
	
							if(searchbyValueB(symmap,uppercase(labelnum.str()))){
								label = searchbyValueS(symmap,labelnum.str());					
							}else{
								label = " ";
							}
							printtextrecord(address,label,opcode,instruction,fullopcode,xflag,iflag,nflag,eflag,ltorg,BASE);
				
							address = address +3;
							current += 6;
						}

					
					}	
				

//Check for RESW and RESB
				vector<string> valuevectest;
				stringstream conversion;
				int symval;
				int num;
				for(const auto &s : valuevecstring){
					//cout<<" The string is: " <<s<<endl;
					num = inttohex(3,true,s);
					valuevec.push_back(num);
					//cout<<" The value is: " <<num<<endl;
				}
				//cout<< "The address is: "<<address<<endl;
				for(int i=0; i<=valuevec.size(); i++){
					int firstnum;
					int secondnum;
					if(address <= valuevec[i]){
						if((i+1)<valuevec.size()){
							firstnum = valuevec[i+1] - valuevec[i];
							if(firstnum%3==0){
								secondnum = firstnum/3;
							}else{
								secondnum = firstnum /1;
							}		
							stringstream RESinstruction;						
                            RESinstruction<<setw(6)<<setfill('0')<<hex<<address;	
							if(searchbyValueB(symmap,uppercase(RESinstruction.str()))){						//check if symbol
								label = searchbyValueS(symmap,uppercase(RESinstruction.str()));
							}
							cout<< setfill('0')<<setw(4)<<right<<hex<< address;
							cout<< setfill(' ')<<setw(4)<<" "; 
							cout<<setfill(' ')<<setw(7)<<left<<"RESW";
							cout<<setfill(' ')<<setw(1)<<" ";
							cout<< setfill(' ')<<setw(6)<<left<<label;
							cout<< setfill(' ')<<setw(2)<<" ";
							cout<<setfill(' ')<<setw(8)<<left<<setbase(10)<<secondnum<<endl;
							address = address + firstnum;
						}
					}else if(i == valuevec.size()){  
						int endaddnum = inttohex(3,true,endadd);
						firstnum = endaddnum - address;
						if(firstnum%3==0){
							secondnum = firstnum/3;
						}else{
							secondnum = firstnum /1;
						}
						
						stringstream RESinstruction;						
						RESinstruction<<setw(6)<<setfill('0')<<hex<<address;	
						if(searchbyValueB(symmap,uppercase(RESinstruction.str()))){						//check if symbol
							label = searchbyValueS(symmap,uppercase(RESinstruction.str()));
						}

							cout<< setfill('0')<<setw(4)<<right<<hex<< address;
							cout<< setfill(' ')<<setw(4)<<" "; 
							cout<<setfill(' ')<<setw(7)<<left<<"RESW";
							cout<<setfill(' ')<<setw(2)<<" ";
							cout<< setfill(' ')<<setw(6)<<left<<label;
							cout<< setfill(' ')<<setw(2)<<" ";
							cout<<setfill(' ')<<setw(8)<<left<<setbase(10)<<secondnum<<endl;
					}		
				}

				} //END RECORD
				if(filecontents.at(0) == 'M'){
					//cout<<"This is the modifiy record."<<endl<<filecontents<<endl;
					string modadd = filecontents.substr(1,6);
					string modlength = filecontents.substr(7,2);
					//cout<<"The address we are modifying is: "<<modadd<<"."<<endl;
					//cout<<"The length we are modifying is: "<<modlength<<"."<<endl<<endl;	
				}
			
				if(filecontents.at(0) == 'E'){
					string endexecution = filecontents.substr(1,6);
					string endinstruct;

					if(searchbyValueB(symmap,uppercase(endexecution))){						//check if symbol
						endinstruct = searchbyValueS(symmap,uppercase(endexecution));
					}
						cout<< setfill(' ')<<setw(8)<<" ";
						cout<< setfill(' ')<<setw(6)<<left<<"END";
						cout<< setfill(' ')<<setw(2)<<" ";
						cout<<setfill(' ')<<setw(8)<<left<<endinstruct<<endl;
					
				}
				
			}	
		}
	}
 
}
