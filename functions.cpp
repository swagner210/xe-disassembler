#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <bitset>
#include <cctype> //tolower
#include <algorithm>
#include <cmath> //exponent
#include "opcode.cpp"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
string opcodefinder(string temp){
	//cout<< "The code starts at: " << temp <<endl;
	if (temp.at(1) == '2' || temp.at(1) == '1' || (temp.at(1) == '3')){
		temp.at(1) = '0';
		//cout << example << endl;
	}					
	if (temp.at(1) == '4' || temp.at(1) == '5' || temp.at(1) == '6' || temp.at(1) == '7'){
		temp.at(1) = '4';
		//cout << temp << endl;
	}
	if (temp.at(1) == '8' || temp.at(1) == '9' || temp.at(1) == 'A' || temp.at(1) == 'B'){
		temp.at(1) = '8';
		//cout << temp << endl;
	}
	if (temp.at(1) == 'C' || temp.at(1) == 'D' || temp.at(1) == 'E' || temp.at(1) == 'F'){
		temp.at(1) = 'C';
		//cout << temp << endl;
	}
	//cout<< " The code is now: "<< temp<<endl;
	if(searchbyValueB(opcodetable, temp)){
		//cout<<"the opcode is: "<<  searchbyValueS(opcodetable, temp)<<endl;
		return temp;
	}else{
		//cout<<"not found"<<endl;
		string error = "error.opcode not found";
		return error;
	}
	//return temp;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool nflagset(string example){
    if (example.at(1) == '2' || example.at(1) == '6' || example.at(1) == 'A' || example.at(1) == 'E'){
        //this is n flag
		return true;
    }
	return false;
}

bool iflagset(string example){
    if (example.at(1) == '1' || example.at(1) == '5' || example.at(1) == '9' || example.at(1) == 'D'){
    	//this is i flag 
		return true;
    }
	return false;
}

bool inflagsset(string example){
    if (example.at(1) == '3' ||example.at(1) == '7' || example.at(1) == 'B' || example.at(1) == 'F') {
    	//this is both
		return true;
    }
	return false;
}

bool xflagset(string binaryxbpe){
    if( binaryxbpe.at(0) == '1'){
		return true;
    }
	return false;
}
bool bflagset(string binaryxbpe){
    if( binaryxbpe.at(1) == '1'){
		return true;
    }
	return false;
}
bool pflagset(string binaryxbpe){
    if( binaryxbpe.at(2) == '1'){
		return true;
    }
	return false;
}
bool eflagset(string binaryxbpe){
    if( binaryxbpe.at(3) == '1'){
		return true;
    }
	return false;
}

string strtobinary(char A){
	if(A == '0'){return "0000";}
	if(A == '1'){return "0001";}
	if(A == '2'){return "0010";}
	if(A == '3'){return "0011";}
	if(A == '4'){return "0100";}
	if(A == '5'){return "0101";}
	if(A == '6'){return "0110";}
	if(A == '7'){return "0111";}
	if(A == '8'){return "1000";}
	if(A == '9'){return "1001";}
	if(A == 'A'){return "1010";}
	if(A == 'B'){return "1011";}
	if(A == 'C'){return "1100";}
	if(A == 'D'){return "1101";}
	if(A == 'E'){return "1110";}
	if(A == 'F'){return "1111";}				
}

int stringtoint(char A){
	if(A == '0'){return 0;}
	if(A == '1'){return 1;}
	if(A == '2'){return 2;}
	if(A == '3'){return 3;}
	if(A == '4'){return 4;}
	if(A == '5'){return 5;}
	if(A == '6'){return 6;}
	if(A == '7'){return 7;}
	if(A == '8'){return 8;}
	if(A == '9'){return 9;}
	if(A == 'A'){return 10;}
	if(A == 'B'){return 11;}
	if(A == 'C'){return 12;}
	if(A == 'D'){return 13;}
	if(A == 'E'){return 14;}
	if(A == 'F'){return 15;}				
}

int inttohex(int format,bool neg, string str){
	//cout<<"the string we recieved was: "<< str<<endl;
	char temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8;
	int temp1int,temp2int,temp3int,temp4int,temp5int,temp6int, temp7int, temp8int, number;
	temp1 = str.at(0);
	temp2 = str.at(1);
	temp3 = str.at(2);
	
	temp1int = stringtoint(temp1);
	temp2int = stringtoint(temp2);
	temp3int = stringtoint(temp3);

    if (format == 3){
		if(neg){
        	temp4 = str.at(3);
	    	temp5 = str.at(4);
			temp6 = str.at(5);
			temp4int = stringtoint(temp4);
			temp5int = stringtoint(temp5);
			temp6int = stringtoint(temp6);
			number = (temp1int * pow(16,5)) + (temp2int * pow(16,4)) + (temp3int * pow(16,3)) + (temp4int * pow(16,2)) + (temp5int * 16) + temp6int;
		}else{			
	    	number = (temp1int * pow(16,2)) + (temp2int * 16) + temp3int;
		}
    }
    if (format == 4){
        temp4 = str.at(3);
	    temp5 = str.at(4);

		/*if(neg){
        	temp6 = str.at(3);
	    	temp7 = str.at(4);
			temp8 = str.at(5);
			temp6int = stringtoint(temp4);
			temp7int = stringtoint(temp5);
			temp8int = stringtoint(temp6);
*/
        temp4int = stringtoint(temp4);
	    temp5int = stringtoint(temp5);
        number = (temp1int * pow(16,4)) + (temp2int * pow(16,3)) + (temp3int * pow(16,2)) +(temp4int * 16) + temp5int;
	   
    }

	return number;
}

string uppercase(string userinput){
	for(string::iterator k = userinput.begin();k < userinput.end();k++){        //makes so its not case sensitive
			*k = toupper(*k);
	}
	return userinput;
}

string negativenum(int format,string str){
	//cout<<endl<<"This is the negative number function"<<endl;
	//cout<<str<<endl;

	if(format ==3){
		while(str.length()<6){
			str.insert(0,"F");
		}
	}
	if (format ==4){
		while(str.length()<8){
			str.insert(0,"F");
		}
	}	
	//str = str.substr(0,6);
	//cout<<str<<endl<<endl;
	return str;
}

void printtextrecord(int address,string label,string opcode,string instruction,string fullopcode,bool xflag,bool iflag,bool nflag,bool eflag,bool ltorg,bool base){
	cout<< setfill('0')<<setw(4)<<right<< address;
	cout<< setfill(' ')<<setw(4)<<" "; 
	cout<< setfill(' ')<<setw(6)<<left<<label;
	cout<< setfill(' ')<<setw(2)<<" ";
	if(eflag){
		cout<<"+"<<setfill(' ')<<setw(6)<<left<<opcode;
	}else{
		cout<<setfill(' ')<<setw(7)<<opcode;
	}
	cout<<setfill(' ')<<setw(2)<<" ";
	if(iflag){
		cout<<"#";
		cout<<setfill(' ')<<setw(7)<<left<<instruction;
	}else if(nflag){
		cout<<"@";
		cout<<setfill(' ')<<setw(7)<<left<<instruction;
	}else{
		if(ltorg){
			cout<<"="<<setfill(' ')<<setw(8)<<left<<instruction;
		}else if (xflag){
			instruction = instruction + ",X";
			cout<<setfill(' ')<<setw(8)<<left<<instruction;
		}else{
			cout<<setfill(' ')<<setw(8)<<left<<instruction;
		}
	}
	cout<<" "<<setfill(' ')<<setw(8)<<left<<fullopcode<<endl;

	if(ltorg){
		cout<< setfill('0')<<setw(4)<<right<< address;
		cout<< setfill(' ')<<setw(4)<<" "; 
		cout<<setfill(' ')<<setw(6)<<label;
		cout<<setfill(' ')<<setw(2)<<" ";
		cout<<setfill(' ')<<setw(7)<<left<<"LTORG"<<endl;
	}
	if(base){
		cout<< setfill('0')<<setw(4)<<right<< address;
		cout<< setfill(' ')<<setw(4)<<" "; 
		cout<<setfill(' ')<<setw(6)<<label;
		cout<<setfill(' ')<<setw(2)<<" ";
		cout<<setfill(' ')<<setw(7)<<left<<"BASE";
		cout<<setfill(' ')<<setw(2)<<" ";
		cout<<setfill(' ')<<setw(24)<<left<<instruction<<endl;

}


}
