#include <iostream>
#include <map>
#include <string>
#include <iterator>
#include "opcode.h"

using namespace std;

string searchbyValueS(map<string, string> &opcodetable, string val){
	map<string, string> ::iterator iter = opcodetable.begin();
		while(iter!= opcodetable.end()){
			if(iter -> second == val){
				return iter->first;
				
			}
			iter++;
		}
}

bool searchbyValueB(map<string, string> &opcodetable, string val){
	map<string, string> ::iterator iter = opcodetable.begin();
		while(iter!= opcodetable.end()){
			if(iter -> second ==val){
				return true;
				
			}
			iter++;
		}
	return false;
}


//opcode::opcode(){
	/*//map<string , string> opcodetable = {
	opcodetable.insert(make_pair("ADD", "18"));
	opcodetable.insert(make_pair("ADDF", "58"));
	opcodetable.insert(make_pair("ADDR", "90"));
	opcodetable.insert(make_pair("AND", "40"));
	opcodetable.insert(make_pair("CLEAR", "B4"));
	opcodetable.insert(make_pair("COMP", "28"));
	opcodetable.insert(make_pair("COMPF", "88"));
	opcodetable.insert(make_pair("COMPR", "A0"));
	opcodetable.insert(make_pair("DIV", "24"));
	opcodetable.insert(make_pair("DIVF", "64"));
	opcodetable.insert(make_pair("DIVR", "9C"));
	opcodetable.insert(make_pair("FIX", "C4"));
	opcodetable.insert(make_pair("FLOAT", "C0"));
	opcodetable.insert(make_pair("HIO", "F4"));
	opcodetable.insert(make_pair("J", "3C"));
	opcodetable.insert(make_pair("JEQ", "30"));
	opcodetable.insert(make_pair("JGT", "34"));
	opcodetable.insert(make_pair("JLT", "38"));
	opcodetable.insert(make_pair("JSUB", "48"));
	opcodetable.insert(make_pair("LDA", "00"));
	opcodetable.insert(make_pair("LDB", "68"));
	opcodetable.insert(make_pair("LDCH", "50"));
	opcodetable.insert(make_pair("LDF", "70"));
	opcodetable.insert(make_pair("LDL", "08"));
	opcodetable.insert(make_pair("LDS", "6C"));
	opcodetable.insert(make_pair("LDT", "74"));
	opcodetable.insert(make_pair("LDX", "04"));
	opcodetable.insert(make_pair("LPS", "D0"));
	opcodetable.insert(make_pair("MUL", "20"));
	opcodetable.insert(make_pair("MULF", "60"));
	opcodetable.insert(make_pair("NORM", "C8"));
	opcodetable.insert(make_pair("OR", "44"));
	opcodetable.insert(make_pair("RD", "D8"));
	opcodetable.insert(make_pair("RMO", "AC"));
	opcodetable.insert(make_pair("RSUB", "4C"));
	opcodetable.insert(make_pair("SHIFTL", "A4"));
	opcodetable.insert(make_pair("SHIFTR", "A8"));
	opcodetable.insert(make_pair("SIO", "F0"));
	opcodetable.insert(make_pair("SSK", "E6"));
	opcodetable.insert(make_pair("STA", "0C"));
	opcodetable.insert(make_pair("STB", "78"));
	opcodetable.insert(make_pair("STCH", "54"));
	opcodetable.insert(make_pair("STF", "80"));
	opcodetable.insert(make_pair("STI", "D4"));
	opcodetable.insert(make_pair("STL", "14"));
	opcodetable.insert(make_pair("STS", "7C"));
	opcodetable.insert(make_pair("STSW", "E8"));
	opcodetable.insert(make_pair("STT", "84"));
	opcodetable.insert(make_pair("STX", "10"));
	opcodetable.insert(make_pair("SUB", "1C"));
	opcodetable.insert(make_pair("SUBF", "5C"));
	opcodetable.insert(make_pair("SUBR", "94"));
	opcodetable.insert(make_pair("SBC", "B0"));
	opcodetable.insert(make_pair("TD", "E0"));
	opcodetable.insert(make_pair("TIO", "F8"));
	opcodetable.insert(make_pair("TIX", "2C"));
	opcodetable.insert(make_pair("TIXR", "B8"));
	opcodetable.insert(make_pair("WD", "DC"));
*/


map<string , string> opcodetable = {{
	{"ADD", "18"},
	{"ADDF", "58"},
	{"ADDR", "90"},
	{"AND", "40"},
	{"CLEAR", "B4"},
	{"COMP", "28"},
	{"COMPF", "88"},
	{"COMPR", "A0"},
	{"DIV", "24"},
	{"DIVF", "64"},
	{"DIVR", "9C"},
	{"FIX", "C4"},
	{"FLOAT", "C0"},
	{"HIO", "F4"},
	{"J", "3C"},
	{"JEQ", "30"},
	{"JGT", "34"},
	{"JLT", "38"},
	{"JSUB", "48"},
	{"LDA", "00"},
	{"LDB", "68"},
	{"LDCH", "50"},
	{"LDF", "70"},
	{"LDL", "08"},
	{"LDS", "6C"},
	{"LDT", "74"},
	{"LDX", "04"},
	{"LPS", "D0"},
	{"MUL", "20"},
	{"MULF", "60"},
	{"NORM", "C8"},
	{"OR", "44"},
	{"RD", "D8"},
	{"RMO", "AC"},
	{"RSUB", "4C"},
	{"SHIFTL", "A4"},
	{"SHIFTR", "A8"},
	{"SIO", "F0"},
	{"SSK", "E6"},
	{"STA", "0C"},
	{"STB", "78"},
	{"STCH", "54"},
	{"STF", "80"},
	{"STI", "D4"},
	{"STL", "14"},
	{"STS", "7C"},
	{"STSW", "E8"},
	{"STT", "84"},
	{"STX", "10"},
	{"SUB", "1C"},
	{"SUBF", "5C"},
	{"SUBR", "94"},
	{"SBC", "B0"},
	{"TD", "E0"},
	{"TIO", "F8"},
	{"TIX", "2C"},
	{"TIXR", "B8"},
	{"WD", "DC"}
}};

/*int main{
	string search = "1C";
	if(searchbyValueB(opcodetable, search)){
		cout<<"the code is: "<<  searchbyValueS(opcodetable, search)<<endl;;
	}else{
		cout<<"not found"<<endl;
	}
return 0;
}
*/
